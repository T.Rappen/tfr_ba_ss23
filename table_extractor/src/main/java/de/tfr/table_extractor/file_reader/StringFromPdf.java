package de.tfr.table_extractor.file_reader;

import de.tfr.table_extractor.utils.FileExtensionUtil;
import lombok.extern.java.Log;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.util.logging.Level;

/**
 * This class has been taken from bpi and was modified slightly to fit the needs of this application.
 *
 * @author t.rappen (for the original class in bpi and for this adaptation)
 *
 * Uses Apaches PDFBox to parse PDF documents to DatabaseFiles
 */
@Log
final class StringFromPdf {

  /**
   * Private constructor prohibits initializing as utility classes must not be initialized.
   */
  private StringFromPdf() {
    throw new IllegalStateException("Utility class");
  }

  public static String extractTextFromPdfFile(Path path, boolean positionSort) {
    return extractTextFromPdfFile(path.toFile(), positionSort);
  }

  /**
   * @param file inbound having a pdf-file attached to it
   * @return new DatabaseFile with the contents of the given inbound in its byte-array
   */
  public static String extractTextFromPdfFile(File file, boolean positionSort) {

    if (file == null) {
      log.log(Level.SEVERE, "Input did not pass inspection.");
      System.exit(3);
    } else if (!file.isFile()) {
      log.log(Level.SEVERE, "Input did not pass inspection.");
      System.exit(4);
    } else if (!file.exists()) {
      log.log(Level.SEVERE, "Input did not pass inspection.");
      System.exit(5);
    } else if (!FileExtensionUtil.isPdf(
        Path.of(file.getAbsolutePath()))) {
      log.log(Level.SEVERE, "Input did not pass inspection.");
      System.exit(6);
    }

    try (PDDocument pdDocument = PDDocument.load(file)) {

    PDFTextStripper stripper = new PDFTextStripper();

    stripper.setSortByPosition(positionSort);
    //default TODO decide on if/how to handle this -> stripper.setLineSeparator(System.lineSeparator());

    // automatically closes pdDocument after reading
      return new String(stripper.getText(pdDocument).getBytes(StandardCharsets.UTF_8));
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }
}

