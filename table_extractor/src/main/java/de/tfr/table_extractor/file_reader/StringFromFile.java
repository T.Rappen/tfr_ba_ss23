package de.tfr.table_extractor.file_reader;

import de.tfr.table_extractor.utils.FileExtensionUtil;

import java.io.File;
import java.nio.file.Path;

public class StringFromFile {

  public static String readFromFile(Path pathOfFile) {
    return readFromFile(pathOfFile.toFile());


    //String fileAsString = "";
    //if (FileExtensionUtil.isText(pathOfFile)) {
    //  fileAsString = StringFromTxt.readFileToString(pathOfFile);
    //} else if (FileExtensionUtil.isPdf(pathOfFile)) {
    //  fileAsString = StringFromPdf.extractTextFromPdfFile(pathOfFile, false);
    //}
    //return fileAsString;
  }

  public static String readFromFile(File file) {
    String fileAsString = "";
    if (FileExtensionUtil.isText(file.getName())) {
      fileAsString = StringFromTxt.readFileToString(file);
    } else if (FileExtensionUtil.isPdf(file.getName())) {
      fileAsString = StringFromPdf.extractTextFromPdfFile(file, false);
    }
    return fileAsString;
  }

}
