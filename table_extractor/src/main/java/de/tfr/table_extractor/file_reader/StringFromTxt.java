package de.tfr.table_extractor.file_reader;

import de.tfr.table_extractor.utils.FileExtensionUtil;
import lombok.extern.java.Log;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

@Log
class StringFromTxt {

  /**
   * Private constructor prohibits initializing as utility classes must not be initialized.
   */
  private StringFromTxt(){
    throw new IllegalStateException("Utility class");
  }

  public static String readFileToString(File file) {

    try {

      BufferedReader bufferedReader = new BufferedReader(new FileReader(file));

      StringBuilder stringBuilder = new StringBuilder();

      String currentLine;
      while ((currentLine = bufferedReader.readLine()) != null) {
        stringBuilder.append(currentLine);
      }

      log.info("File read successfully");
      return stringBuilder.toString();

    } catch (IOException e) {
      log.log(java.util.logging.Level.SEVERE, "Error while reading file. Not recoverable.", e);
      System.exit(9);
      // Useless throw. Won't happen after System.exit...
      throw new RuntimeException(e);
    }

  }
}
