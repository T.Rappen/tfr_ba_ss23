package de.tfr.table_extractor;

import de.tfr.table_extractor.data_objects.RuntimeParameters;
import de.tfr.table_extractor.data_objects.TableTransferObject;
import de.tfr.table_extractor.extractor.TableFromText;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.env.ConfigurableEnvironment;

@SpringBootApplication
public class TableExtractorApplication {

  public static void main(String[] args) {
    ConfigurableApplicationContext applicationContext = SpringApplication.run(TableExtractorApplication.class, args);

    RuntimeParameters runtimeParameters = loadRuntimeParameters(args, applicationContext.getEnvironment());

    TableTransferObject tableTransferObject = TableFromText.extractTableX(runtimeParameters).orElseThrow();

    //System.out.println(tableTransferObject.table);
  }

  private static RuntimeParameters loadRuntimeParameters(
      String[] args, ConfigurableEnvironment environment
  ) {
    RuntimeParameters runtimeParameters = new RuntimeParameters();

    // For adding the parameter in the console as an argument
    if (args != null && args.length == 1) {
      runtimeParameters.setPathOfTestFile(args[0]);
    } else {
      runtimeParameters.setPathOfTestFile(environment.getProperty("preExtraction.pathOfTestFile", String.class));
    }

    runtimeParameters.setInputLineBreakSymbol(
        environment.getProperty("periExtraction.inputLineBreakSymbol", String.class, "\r\n"));

    runtimeParameters.setDropLinesWithoutNumbers(
        environment.getProperty("periExtraction.dropLinesWithoutNumbers", Boolean.class, false));

    runtimeParameters.setMinimumGroupSize(environment.getProperty("minimumGroupSize", Integer.class, 2));

    return runtimeParameters;
  }



}
