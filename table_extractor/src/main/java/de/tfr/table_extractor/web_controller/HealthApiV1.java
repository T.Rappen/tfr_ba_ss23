package de.tfr.table_extractor.web_controller;

import lombok.extern.java.Log;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@Log
@RestController
public class HealthApiV1 {

  @PostMapping("/api/v1/health")
  public ResponseEntity<byte[]> checkHealth(@RequestBody MultipartFile file) throws IOException {
    log.info("File \"" + file.getOriginalFilename() + "\" received and returned.");
    return ResponseEntity.ok().contentType(MediaType.APPLICATION_PDF).body(file.getBytes());
  }

}
