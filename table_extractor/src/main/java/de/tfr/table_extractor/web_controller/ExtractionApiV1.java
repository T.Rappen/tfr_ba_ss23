package de.tfr.table_extractor.web_controller;

import de.tfr.table_extractor.data_objects.TableTransferObject;
import lombok.extern.java.Log;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

@Log
@RestController
public class ExtractionApiV1 {

  @PostMapping("/api/v1/extract")
  public ResponseEntity<TableTransferObject> extractTable(HttpServletRequest request) throws IOException {

      InputStream inputStream = request.getInputStream();
      ByteArrayOutputStream out = new ByteArrayOutputStream();
      byte[] fileBytes = out.toByteArray();
      // process the fileBytes here
      return ResponseEntity.ok().body(new TableTransferObject());

  }

}
