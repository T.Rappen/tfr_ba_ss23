package de.tfr.table_extractor.utils;

import java.nio.file.Path;
import java.util.Optional;

public class FileExtensionUtil {

  public static Optional<String> getExtension(String file) {
    return Optional.ofNullable(file)
        .filter(f -> f.contains("."))
        .map(f -> f.substring(file.lastIndexOf(".") + 1));
  }

  public static Optional<String> getExtension(Path path) {
    return getExtension(path.getFileName().toString());
  }

  public static boolean isPdf(Path filename) {
    return getExtension(filename).orElse("").equals("pdf");
  }

  public static boolean isPdf(String filename) {
    return getExtension(filename).orElse("").equals("pdf");
  }

  public static boolean isDoc(String filename) {
    return getExtension(filename).orElse("").equals("doc");
  }

  public static boolean isText(Path filename) {
    return getExtension(filename).orElse("").equals("txt");
  }

  public static boolean isText(String filename) {
    return getExtension(filename).orElse("").equals("txt");
  }

}
