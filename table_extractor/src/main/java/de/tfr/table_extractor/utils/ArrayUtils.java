package de.tfr.table_extractor.utils;

import de.tfr.table_extractor.data_objects.CharacterClass;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

public class ArrayUtils {

  private static final Log log = LogFactory.getLog(ArrayUtils.class);

  public static String stringOf2DArray(String[][] array) {
    StringBuilder sb = new StringBuilder();
    for (String[] strings : array) {
      for (String string : strings) {
        sb.append(string).append(" ");
      }
      sb.append("\n");
    }
    return sb.toString();
  }

  public static String stringOf2DArray(CharacterClass[][] array) {
    StringBuilder sb = new StringBuilder();
    for (CharacterClass[] strings : array) {
      for (CharacterClass string : strings) {
        sb.append(string).append(" ");
      }
      sb.append("\n");
    }
    return sb.toString();
  }

  public static String[] removeLinesWithoutNumbers(String[] inputLines) {
    log.info("Removing lines without numbers in them.");

    Pattern pattern = Pattern.compile(".*\\d.*");
    List<String> reducedLines = new ArrayList<>();
    int removedLines = 0;

    for (int i = 0; i < inputLines.length; i++) {
      String line = inputLines[i];
      if (pattern.matcher(line).matches()) {
        reducedLines.add(inputLines[i]);
      } else {
        removedLines++;
      }
    }

    log.info("Successfully removed " + removedLines + "/" + inputLines.length + " lines without numbers.");
    return reducedLines.toArray(new String[0]);
  }

  public static String characterClassArrayToString(CharacterClass[] characterClassArray) {
    StringBuilder stringBuilder = new StringBuilder();
    for (CharacterClass characterClass : characterClassArray) {
      stringBuilder.append(characterClass.toString());
    }
    return stringBuilder.toString();
  }
}
