package de.tfr.table_extractor.utils;

import de.tfr.table_extractor.data_objects.RuntimeParameters;
import org.springframework.core.env.ConfigurableEnvironment;

public class PropertyLoader {

  public RuntimeParameters loadRuntimeParameters(
      String[] args,
      ConfigurableEnvironment environment
  ) {
    RuntimeParameters runtimeParameters = new RuntimeParameters();

    // For adding the parameter in the console as an argument
    runtimeParameters.setPathOfTestFile(args != null && args.length == 1
        ? args[0]
        : environment.getProperty("preExtraction.pathOfTestFile", String.class));

    runtimeParameters.setInputLineBreakSymbol(
        environment.getProperty("periExtraction.inputLineBreakSymbol", String.class, "\r\n"));

    runtimeParameters.setDropLinesWithoutNumbers(
        environment.getProperty("periExtraction.dropLinesWithoutNumbers", Boolean.class, false));

    runtimeParameters.setMinimumGroupSize(environment.getProperty("minimumGroupSize", Integer.class, 2));

    return runtimeParameters;
  }

}
