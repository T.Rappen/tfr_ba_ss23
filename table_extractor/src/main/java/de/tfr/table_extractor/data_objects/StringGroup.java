package de.tfr.table_extractor.data_objects;

import jdk.jshell.spi.ExecutionControl;

import java.util.List;
import java.util.Optional;

public class StringGroup {

  private double sumOfScore;

  /** Optional is used a cache here to calcualte the average just once **/
  private Optional<String> groupStringAverage = Optional.empty();

  private List<String> strings;

  public void addToSumOfScore(double score) {
    this.sumOfScore += score;
  }

  /**
   * Calculate the average string of the group.
   * @param useCachedValueIfAvailable If true, the cached value will be used if available. Be aware: This getter might change th object state if called with "false" here.
   * @return
   */
  public String getGroupStringAverage(boolean useCachedValueIfAvailable) {
    if (groupStringAverage.isEmpty() || !useCachedValueIfAvailable) {
      // Update or create a cached value and return it
      groupStringAverage = Optional.of(calculateGroupStringAverage());
    }
    return groupStringAverage.get();
  }

  private String calculateGroupStringAverage() {
    return "";
  }

}
