package de.tfr.table_extractor.data_objects;

public enum CharacterClass {
  BACKWARD_SLASH,
  CAPITAL_CHARACTER,
  COLON,
  COMMA,
  CURRENCY_SYMBOL,
  DOT,
  FORWARD_SLASH,
  LOWERCASE_CHARACTER,
  NUMBER,
  OTHER,
  TAB,
  WHITESPACE;

  @Override
  public String toString() {
    return name().substring(0,2) + ' ';
  }
}
