package de.tfr.table_extractor.data_objects;

import lombok.*;
import lombok.experimental.Accessors;
import lombok.extern.java.Log;

import java.nio.file.Path;
import java.util.logging.Level;

/*
TODO if it turns out that all setters are able to validate the input indvidually (no dependencys on other values) quit the schinnanigans and remove the validated variable and have automatic @Getter
 */


@Setter
@Getter
@Log
@Accessors(chain = true)
@NoArgsConstructor
public class RuntimeParameters {

  private Path pathOfTestFile;

  private String inputLineBreakSymbol;

  private boolean dropLinesWithoutNumbers;

  private int minimumGroupSize;

  public RuntimeParameters(
      String pathOfTestFile, String inputLineBreakSymbol, boolean dropLinesWithoutNumbers, int minimumGroupSize
  ) {
    this.minimumGroupSize = minimumGroupSize;
    this.inputLineBreakSymbol = inputLineBreakSymbol;
    this.dropLinesWithoutNumbers = dropLinesWithoutNumbers;
    setPathOfTestFile(pathOfTestFile);
  }

  private void validate() {
    if (pathOfTestFile == null) {
      log.log(
          java.util.logging.Level.SEVERE,
          "No \"pathOfTestFile\" given in properties and no filepath given as argument. Not recoverable."
      );
      System.exit(1);
    }
  }

  public void setPathOfTestFile(String pathOfTestFile) {
    Path path = Path.of(pathOfTestFile);
    if (!path.toFile().exists()) {
      log.log(Level.SEVERE, "The given path of the test file does not exist.");
      System.exit(10);
    }
    this.pathOfTestFile = path;
  }

  public void setInputLineBreakSymbol(String inputLineBreakSymbol) {
    this.inputLineBreakSymbol = inputLineBreakSymbol;
  }

  public void setDropLinesWithoutNumbers(boolean dropLinesWithoutNumbers) {
    this.dropLinesWithoutNumbers = dropLinesWithoutNumbers;
  }

  public void setMinimumGroupSize(int minimumGroupSize) {
    if (minimumGroupSize == 1) {
      log.log(
          Level.SEVERE,
          "A group size on one means that all lines are groups by themselves. This cant be what you want."
      );
    }

    if (minimumGroupSize < 1) {
      log.log(Level.SEVERE, "Zero or negative group sizes are not allowed.");
      System.exit(2);
    }

    this.minimumGroupSize = minimumGroupSize;
  }



}
