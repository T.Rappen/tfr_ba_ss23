package de.tfr.table_extractor.extractor;

import de.tfr.table_extractor.data_objects.CharacterClass;
import de.tfr.table_extractor.data_objects.RuntimeParameters;
import de.tfr.table_extractor.data_objects.TableTransferObject;
import de.tfr.table_extractor.file_reader.StringFromFile;
import de.tfr.table_extractor.utils.ArrayUtils;
import de.tfr.table_extractor.utils.StringComparer;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.regex.Pattern;

public class TableFromText {

  // [Row][Column]

  private static final Log log = LogFactory.getLog(TableFromText.class);

  private static final Pattern currencySymbol = Pattern.compile("\\p{Sc}");

  private TableFromText() {
    // Utility classes must not be initialized!
  }

  public static Optional<TableTransferObject> extractTableX(RuntimeParameters runtimeParameters) {

    String inputString = StringFromFile.readFromFile(runtimeParameters.getPathOfTestFile());

    String splitchar = runtimeParameters.getInputLineBreakSymbol();

    if ("system".equals(splitchar)) {
      splitchar = System.lineSeparator();
    }

    String[] inputLines = inputString.split(splitchar);

    // Overwrite array early to keep line numbers consistent later
    if (runtimeParameters.isDropLinesWithoutNumbers()) {
      inputLines = ArrayUtils.removeLinesWithoutNumbers(inputLines);
    }

    var characterClasses = TableFromText.extractTable(inputLines);
    double[] scores = TableFromText.calculateScores(characterClasses);

    double sumOfScore = 0;
    int numberOfLines = 0;

    List<List<String>> groups = new ArrayList<>();

    List<String> group = new ArrayList<>();
    groups.add(group);
    for (int i = 1; i < characterClasses.length - 1; i++) {
      if (scores[i] > 0.8 || scores[i - 1] > 0.925) {
        sumOfScore += scores[i];
        numberOfLines++;
        group.add(inputLines[i]);
      } else {
        group = new ArrayList<>();
        groups.add(group);
      }
    }

    int minimumGroupSize = runtimeParameters.getMinimumGroupSize();

    int groupIndex = 0;
    while (groupIndex < groups.size()) {
      if (groups.get(groupIndex).size() <= minimumGroupSize) {
        groups.remove(groupIndex);
        groupIndex--;
      }
      groupIndex++;
    }

    StringBuilder result = new StringBuilder();

    for (List<String> groupToPrint : groups) {
      for (String line : groupToPrint) {
        result.append(line).append("\n");
      }
    }

    // Delete last trailing newline
    if (result.length() > 0 && result.toString().endsWith("\n")) {
      result.delete(result.length() - 1, result.length());
    }
    /*
    Alle Gruppen zusammen in eigene Listen werfen
    Wenn eine Liste kürzer als x Stellen ist:
      Wenn Match zu den anderen Listen (Durchschnitt über Liste berechnen)
     */

    TableTransferObject tableTransferObject = new TableTransferObject();
    tableTransferObject.table = result.toString();
    tableTransferObject.precision = sumOfScore / numberOfLines;

    if (tableTransferObject.table.length() == 0) {
      return Optional.empty();
    }
    return Optional.of(tableTransferObject);
  }

  public static CharacterClass[][] extractTable(String input) {
    return extractTable(input.split("\r\n"));
  }

  public static CharacterClass[][] extractTable(String[] inputLines) {
    log.info("Extraction of table started.");

    CharacterClass[][] classifiedCharacters = classifyCharacters(inputLines);

    log.info("Extraction of table finished.");
    return classifiedCharacters;
  }

  static CharacterClass[][] classifyCharacters(String[] inputLines) {
    CharacterClass[][] classifiedCharacters = new CharacterClass[inputLines.length][];
    for (int lineIndex = 0; lineIndex < inputLines.length; lineIndex++) {
      String line = inputLines[lineIndex];
      CharacterClass[] lineAsCharacterClasses = classifyLine(line);
      classifiedCharacters[lineIndex] = lineAsCharacterClasses;
    }
    return classifiedCharacters;
  }

  public static CharacterClass[] classifyLine(String line) {
    CharacterClass[] lineAsCharacterClasses = new CharacterClass[line.length()];
    for (int characterIndex = 0; characterIndex < line.length(); characterIndex++) {
      lineAsCharacterClasses[characterIndex] = classifyCharacter(line.charAt(characterIndex));
    }
    return lineAsCharacterClasses;
  }

  static CharacterClass classifyCharacter(char character) {
    if (character >= 'A' && character <= 'Z') {
      return CharacterClass.CAPITAL_CHARACTER;
    }

    if (character >= 'a' && character <= 'z') {
      return CharacterClass.LOWERCASE_CHARACTER;
    }

    if (character >= '0' && character <= '9') {
      return CharacterClass.NUMBER;
    }

    if (character == ' ') {
      return CharacterClass.WHITESPACE;
    }

    if (character == '\t') {
      return CharacterClass.TAB;
    }

    if (character == '/') {
      return CharacterClass.FORWARD_SLASH;
    }

    if (character == '\\') {
      return CharacterClass.BACKWARD_SLASH;
    }

    if (character == '.') {
      return CharacterClass.DOT;
    }

    if (character == ',') {
      return CharacterClass.COMMA;
    }

    if (character == ':') {
      return CharacterClass.COLON;
    }

    if (currencySymbol.matcher(String.valueOf(character)).matches()) {
      return CharacterClass.CURRENCY_SYMBOL;
    }

    return CharacterClass.OTHER;
  }

  public static double[] calculateScores(CharacterClass[][] characterClasses) {
    double[] scores = new double[characterClasses.length];
    for (int i = 0; i < characterClasses.length - 1; i++) {
      scores[i] = StringComparer.levenshteinDistanceNormalized(
          ArrayUtils.characterClassArrayToString(characterClasses[i]),
          ArrayUtils.characterClassArrayToString(characterClasses[i + 1])
      );
    }
    return scores;
  }

}
