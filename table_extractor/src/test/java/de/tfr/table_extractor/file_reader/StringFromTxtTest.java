package de.tfr.table_extractor.file_reader;

import org.junit.jupiter.api.Test;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

import static org.junit.jupiter.api.Assertions.*;

class StringFromTxtTest {

  /**
   * Required filesystem access
   */
  @Test
  void simpleTestWithFilesystem() throws IOException {
    File inputFile = File.createTempFile("testFile", ".txt");
    BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(inputFile));

    bufferedWriter.write("Hallo Welt");

    bufferedWriter.flush();
    bufferedWriter.close();

    assertEquals("Hallo Welt", StringFromFile.readFromFile(inputFile));
  }

  @Test
  void simpleTest() throws IOException {
    Path path = Paths.get("file.txt");
    File inputFile = path.toFile();

    BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(inputFile));

    bufferedWriter.write("Hallo Welt");

    bufferedWriter.flush();
    bufferedWriter.close();

    assertEquals("Hallo Welt", StringFromFile.readFromFile(inputFile));
  }


}
