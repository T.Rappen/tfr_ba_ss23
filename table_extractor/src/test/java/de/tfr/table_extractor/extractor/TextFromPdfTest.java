package de.tfr.table_extractor.extractor;

import de.tfr.table_extractor.file_reader.StringFromFile;
import org.junit.jupiter.api.Test;

import java.nio.file.Path;

import static org.junit.jupiter.api.Assertions.*;

class TextFromPdfTest {

  @Test
  void testBasicTextExctraction() {
    Path pathOfTestFile = Path.of("C:\\Users\\t.rappen\\IdeaProjects\\tfr_ba_ss23\\table_extractor\\src\\test\\resources\\files\\anonymized\\pdf\\A Sample PDF.pdf");

    String contentOfTestfile = """
        Lorem Ipsum\s
        "Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit..."\s
        "There is no one who loves pain itself, who seeks after it and wants to have it, simply because it is pain..."\s
        \s
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris vitae erat nibh. Morbi imperdiet\s
        scelerisque massa, non ornare turpis elementum consectetur. Praesent laoreet vitae libero eget\s
        pulvinar. Fusce malesuada massa at tincidunt tincidunt. Orci varius natoque penatibus et magnis dis\s
        parturient montes, nascetur ridiculus mus. Nam sed tincidunt turpis. Quisque tincidunt dictum augue\s
        sed egestas. Ut scelerisque leo sit amet lectus vehicula, et posuere enim porttitor. Fusce porta varius\s
        elit vel consequat. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos\s
        himenaeos. Quisque in ex libero. Nullam augue mauris, blandit sit amet neque eu, viverra congue est.\s
        Mauris ac auctor dolor. Proin maximus quam id magna vulputate ultricies. Maecenas lacinia dolor\s
        eros, a bibendum tellus bibendum vitae. Praesent vel neque imperdiet, eleifend est vel, pharetra ex.\s
        Vivamus a hendrerit nisl. Etiam dignissim sed arcu in cursus. Pellentesque rutrum semper justo, ut\s
        ornare mi vehicula sodales. Fusce ut imperdiet nisl. Nullam suscipit, lectus et semper ornare, ante nisi\s
        semper lorem, in viverra mauris augue non eros. Nam tincidunt mauris mi, nec congue est bibendum\s
        vel. Morbi ullamcorper eros at tempus suscipit. Nunc mattis sed lectus at eleifend. Morbi convallis\s
        augue metus, accumsan malesuada elit consectetur quis. Donec vel turpis efficitur, malesuada ligula\s
        ut, blandit dui. Integer at purus et quam blandit volutpat. Donec vel orci efficitur, sodales diam nec,\s
        malesuada ipsum.\s
        Nullam euismod, odio in ornare fermentum, nunc sapien vestibulum erat, aliquam elementum est est\s
        sed erat. Proin facilisis lacus vitae magna volutpat, vitae commodo velit volutpat. Aliquam rutrum erat\s
        a nibh elementum, quis eleifend nulla fringilla. Proin sed velit pulvinar est consequat rhoncus ut non\s
        augue. Cras id velit purus. Aliquam convallis venenatis ultrices. Nam pulvinar aliquet magna, at ornare\s
        ligula cursus vel. Curabitur vitae cursus ante. Morbi congue lorem ac ante pretium commodo. Nulla\s
        imperdiet diam eget tortor dignissim egestas vitae sit amet sem.\s
        In purus elit, finibus quis nisi ut, placerat consectetur erat. Pellentesque habitant morbi tristique\s
        senectus et netus et malesuada fames ac turpis egestas. Aenean non metus turpis. Vestibulum at\s
        iaculis massa. Nunc orci magna, congue a egestas nec, vulputate non mauris. Fusce malesuada a\s
        ipsum eu porttitor. Cras pretium porta tempor. Integer pulvinar convallis ipsum at varius. Cras convallis\s
        varius arcu eget egestas. Praesent ullamcorper nisl ex, et mollis ante sagittis vel. Ut elementum ligula\s
        enim. Maecenas massa dui, malesuada in metus nec, vulputate efficitur nibh.\s
        Curabitur dictum lacus magna. Integer ex velit, malesuada eu ligula id, suscipit sagittis lacus.\s
        Phasellus cursus viverra ultrices. Maecenas magna arcu, finibus eget elit vitae, iaculis ornare tellus.\s
        Nullam faucibus libero felis, in efficitur lorem vestibulum id. Aliquam sagittis scelerisque tellus, a\s
        gravida orci suscipit quis. Orci varius natoque penatibus et magnis dis parturient montes, nascetur\s
        ridiculus mus. Pellentesque ac risus arcu. Aliquam condimentum massa sed tincidunt tincidunt.\s
        Pellentesque non mauris in elit rhoncus condimentum. Vivamus tempus lacus ex. Integer malesuada,\s
        justo sed finibus egestas, risus velit porta erat, a tempor sem augue vel leo. Ut nisi massa, egestas\s
        quis orci nec, varius condimentum lorem. Nullam hendrerit feugiat lacinia. Pellentesque habitant morbi\s
        tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum ante ipsum primis in\s
        faucibus orci luctus et ultrices posuere cubilia Curae; \r
        """;

    assertEquals(contentOfTestfile, StringFromFile.readFromFile(pathOfTestFile));
  }

}
