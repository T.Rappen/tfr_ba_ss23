package de.tfr.table_extractor;

import de.tfr.table_extractor.data_objects.RuntimeParameters;
import de.tfr.table_extractor.data_objects.TableTransferObject;
import de.tfr.table_extractor.extractor.TableFromText;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest
class TableExtractorApplicationTests {

  @Test
  void contextLoads() {
  }

  @Test
  void testOfTestFile1() {

    RuntimeParameters runtimeParameters = new RuntimeParameters();
    runtimeParameters.setInputLineBreakSymbol("\r\n");
    runtimeParameters.setDropLinesWithoutNumbers(true);
    runtimeParameters.setPathOfTestFile("src/main/resources/files/originals/text/file_1_original.txt");
    runtimeParameters.setMinimumGroupSize(2);

    Optional<TableTransferObject> returnValue = TableFromText.extractTableX(runtimeParameters);

    assertTrue(returnValue.isPresent());

    // TODO add option to "heal" broken amounts like they are here (the original document had handwritten markings on it

    assertEquals("""
        PUR 2212-0049 14/12/2022 5440066610 9152.00.
        PUR 2212-0050 15/12/2022 5440066644 7040.00
        PUR 2212-0051 15/12/2022 5440066645 18304.00
        PUR 2212-0052 17/12/2022 5440066708 27456.00
        PUR 2212-0053 17/12/2022 5440066775 7040.00
        PUR 2212-0054 19/12/2022 5440066776 27456.00.
        PUR 2212-0055 21/12/2022 5440066945 9152.00
        PUR 2212-0056 22/12/2022 5440066912 27456.00
        PUR 2212-0057 23/12/2022 5440066913 18304.00
        PUR 2212-0058 27/12/2022 5440067032 18304.00,""", returnValue.get().table);

  }

  @Test
  void testOfTestFile2() {

    RuntimeParameters runtimeParameters = new RuntimeParameters();
    runtimeParameters.setInputLineBreakSymbol("\r\n");
    runtimeParameters.setDropLinesWithoutNumbers(true);
    runtimeParameters.setPathOfTestFile("src/main/resources/files/originals/text/file_2_original.txt");
    runtimeParameters.setMinimumGroupSize(2);

    Optional<TableTransferObject> returnValue = TableFromText.extractTableX(runtimeParameters);

    assertTrue(returnValue.isPresent());

    assertEquals("""
        301;1100091236 159,26 15.04.2016 0,00 159,26
        301;1100091237 147,23 15.04.2016 0,00 147,23
        301;1100091238 150,93 15.04.2016 0,00 150,93
        301;1100091239 160,84 15.04.2016 0,00 160,84
        301;1100091240 150,87 15.04.2016 0,00 150,87
        301;1100091241 153,47 15.04.2016 0,00 153,47
        301;1100091242 170,88 15.04.2016 0,00 170,88
        301;1100092310 166,25 15.04.2016 0,00 166,25
        301;1100092311 170,25 18.04.2016 0,00 170,25
        301;1100092312 171,25 18.04.2016 0,00 171,25
        301;1100092313 174,86 18.04.2016 0,00 174,86""", returnValue.get().table);

  }

  @Test
  void  testOfTestFile3() {
    RuntimeParameters runtimeParameters = new RuntimeParameters();
    runtimeParameters.setInputLineBreakSymbol("\r\n");
    runtimeParameters.setDropLinesWithoutNumbers(true);
    runtimeParameters.setPathOfTestFile("src/test/resources/files/originals/pdf/A Sample PDF.pdf");
    runtimeParameters.setMinimumGroupSize(2);
  }

}
