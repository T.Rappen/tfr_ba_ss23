# tfr_ba_ss23

## Problemstellung

Avise werden in BPI mit Hilfe von Mappings in eine Hauseigene Struktur übersetzt. Dabei wird für jede Position in der Avise auch eine Position in der Hauseigenen Struktur geschaffen.

In SmartEye wird aktuell nur die Gesamtsumme gesucht und gefunden, es findet daher in Autobank später zwangsweise KEIN Mapping zwischen Position und offener Position statt. Stattdessen sendet SmartEye den gesamten Inhalt der Nachricht weiter an Autobank, wo dann mithilfe von String-Operation versucht wird, die Positionen zuzuordnen. Bei Unstimmigkeiten zwischen der erwarteten und gefundenen Gesamtsumme muss manuell nach dem Fehler gesucht werden. Ein Zuordnen von Positionen der Avise zu offenen Positionen in SAP würde den manuellen Aufwand deutlich verringern.

## Schritte zur Lösung

Aus den eingehenden Avise müssen die Positionen herausgefiltert werden. Dabei stehen KEINE Informationen zu noch offenen Positionen eines Kunden zur Verfügung.

## Ansätze

1. Deterministisch über einen "KI-Algorythmus", der anhand von Metriken versucht, eine hohe Trefferquote bei gleichzeitig nachvollziehbaren Ergebnissen zu liefern
2. Via Maschine Learning Algorithmus

## TODO

- Alle Gruppen in Listen stecken, die größte Liste bestimmt den Modus der Tabelle. Alle kleineren Gruppen müssen dem Modus folgen. Dazu muss über die gesamte Liste ein "Durchschnittlicher String" geschaffen werden. Andere Listen müssen mit ihrem eigenen Durchschnitt dann dieser Gruppe zu einem Prozentsatz nahe stehen, damit diese auch ausgegeben werden. Dieses Verhalten verhindert, dass kleine Gruppen (ausgegeben werden und sorgt gleichzeitig dafür, dass Gruppen/Zeilen die noch zur Liste grhören, aber auch der nächsten Seite sind, noch ausgegeben werden.
- Die Strings, die die Zeilen repräsentieren zu einem Enum machen
- Den lavendish Algo verstehen und auf Enum-Werte umbauen -> Feste distanzen zwischen den String statt char-reihenfolge
